// https://github.com/miguelbalboa/rfid
#include <SPI.h>
#include <MFRC522.h>
#define RST_PIN         5           // Configurable, see example pin layout in the link above.
#define SS_PIN          53          // Configurable

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

String read_rfid;                   // Add how many you need and don't forget to include the UID.
String allowed_rfid="67ed3d";        // This is for my main RFID Card. aka. The one I will be using to turn on my PC. Can also be used to shut it down if you want to.
int lock = 7;                      // Relay pin

// Initialize.
void setup() {
    Serial.begin(9600);         // Initialize serial communications with the PC
    while (!Serial);            // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();                // Init SPI bus
    mfrc522.PCD_Init();         // Init MFRC522 card
    pinMode(lock, OUTPUT);
}

// Helper routine to dump a byte array as hex values to Serial.
void dump_byte_array(byte *buffer, byte bufferSize) {
    read_rfid="";
    for (byte i = 0; i < bufferSize; i++) {
        read_rfid=read_rfid + String(buffer[i], HEX);
    }
}

void grand_access() {
  Serial.println(" - Access granted.");
  digitalWrite(LED_BUILTIN,HIGH);
  digitalWrite(lock,LOW);
  delay(4000);
  digitalWrite(LED_BUILTIN,LOW);
}

void deny_access() {
  Serial.println(" - Access denied!");
  for (int i = 0; i <= 4; i++) {
    digitalWrite(LED_BUILTIN,HIGH);
    delay(200);
    digitalWrite(LED_BUILTIN,LOW);
    delay(200);
  }
}

void loop() {
    digitalWrite(lock,HIGH);
    if ( ! mfrc522.PICC_IsNewCardPresent()) return;
    if ( ! mfrc522.PICC_ReadCardSerial()) return;
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.print(read_rfid);
    if (read_rfid==allowed_rfid) {
      grand_access();
    }
    else {
      deny_access();
    }
}